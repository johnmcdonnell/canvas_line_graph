/*
 * Author: John McDonnell
 * Student Number: D00096987
 * Email: D0009687@student.dkit.ie
 * Course: Computing
 * Module: Rich Web Applications 1
 * CA 1
 * Due Date 15th of November 2014
 * Version Final
 */



function draw()
{
    var margin = 50;
    var ctx = drawCanvas();
    function drawCanvas()
    {
        var c = document.getElementById("myCanvas");
        var ctx = c.getContext("2d");

        c.width = 700;
        c.height = 500;


        ctx.font = '200 12px Arial';

        return ctx;

    }


    var graph = createGraph(ctx);
    function createGraph(ctx)
    {
        grid();
        function grid()
        {

            ctx.strokeStyle = "gold";
            /*
             *Creates background grid 
             *
             */
            var width = 700;
            var height = 500;
            for (var x = 0; x <= height; x += 50)
            {
                ctx.moveTo(margin, 0.5 + x + margin);
                ctx.lineTo(width - margin, 0.5 + x + margin);
            }

            ctx.strokeStyle = "gold";
            ctx.stroke();
        }
        ctx.strokeStyle = "white";


        var graphHeight = ctx.canvas.height - margin;
        var graphWidth = ctx.canvas.width - margin;
        ctx.beginPath();

        //Arrays

        var yLabel;
        //Padding around labels
        var xLabelPadding = 20;
        var yLabelPadding = 20;

        var xAxisMargin = graphWidth / data.length;
        var min = data[0];
        var max = data[0];


        /*
         *finding min and max in the Array
         */
        for (var k = 0; k < data.length; k++)
        {
            if (min > data[k])
            {
                min = data[k];
            }
            if (max < data[k])
            {
                max = data[k];
            }
        }


        /*
         *check to see if loop above is working correctly
         *yes - Check Complete.
         */

        var median = (max * 1.1) - min; //1.1 is scaling 
        var yAxisRange = 20;
        var yAxisMargin = median / yAxisRange;
        //alert(min);
        // alert(max);
        // alert(median);
        var pixels = graphHeight / median;
        ctx.moveTo(margin, graphHeight - (data[0] - min) * pixels);


        /*
         *calculating x axis along with plotting graph
         */

        for (var i = 0; i < data.length; i++)
        {
            /*
             * Plot lines on graph
             */
            ctx.lineWidth = 2;
            ctx.lineTo(margin + (xAxisMargin * i), graphHeight - (data[i] - min) * pixels);

            /*
             * X Axis lables
             */
            ctx.fillStyle = "gold";

            ctx.fillText(xLabel[i], (margin - 8) + (xAxisMargin * i), graphHeight + xLabelPadding);
            /*
             * Plot point values on graph and scale values off each point
             */

            ctx.fillStyle = "white";

            ctx.fillText(data[i], margin + 5 + (xAxisMargin * i), graphHeight - (data[i] - min) * pixels - 8);

            ctx.fillStyle = "red";

            ctx.fillRect(margin + (xAxisMargin * i) - 1, graphHeight - (data[i] - min) * pixels - 1, 5, 5);

            ctx.stroke();

        }


        // alert(data2);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(margin, graphHeight - (data2[0] - min) * pixels);
        for (var q = 0; q < data2.length; q++)
        {
            /*
             * Plot lines on graph
             */
            // alert(data2[q]);
            ctx.strokeStyle = "red";
            ctx.lineTo(margin + (xAxisMargin * q), graphHeight - (data2[q] - min) * pixels);

            /*
             * Plot point values on graph and scale values off each point
             */

            ctx.fillStyle = "red";

            ctx.fillText(data2[q], margin + 5 + (xAxisMargin * q), graphHeight - (data2[q] - min) * pixels - 8);
            ctx.fillStyle = "gold";

            ctx.fillStyle = "white";
            ctx.fillRect(margin + (xAxisMargin * q) - 1, graphHeight - (data2[q] - min) * pixels - 1, 5, 5);
            ctx.fillStyle = "gold";
            ctx.stroke();
        }

        /*
         * google sales plot
         */
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(margin, graphHeight - (data3[0] - min) * pixels);
        for (var s = 0; s < data3.length; s++)
        {
            /*
             * Plot lines on graph
             */
            // alert(data2[q]);
            ctx.strokeStyle = "green";
            ctx.lineTo(margin + (xAxisMargin * s), graphHeight - (data3[s] - min) * pixels);

            /*
             * Plot point values on graph and scale values off each point
             */

            ctx.fillStyle = "yellow";

            ctx.fillText(data3[s], margin + 5 + (xAxisMargin * s), graphHeight - (data3[s] - min) * pixels - 8);
            ctx.fillStyle = "green";

            ctx.fillStyle = "yellow";
            ctx.fillRect(margin + (xAxisMargin * s) - 1, graphHeight - (data3[s] - min) * pixels - 1, 5, 5);
            ctx.fillStyle = "gold";
            ctx.stroke();
        }



        /*
         *calculating y axis along with plotting graph
         */
        for (var x = 0; x < yAxisRange; x++)
        {
            /*
             * Y Axis lables
             */

            ctx.fillText(Math.round(min + (x * yAxisMargin)), (margin - 8) - yLabelPadding, graphHeight - (x * graphHeight / yAxisRange));
        }

        ctx.stroke();

        /*
         * Draw X axis
         */
        ctx.strokeStyle = "white";
        ctx.beginPath();
        ctx.moveTo(margin, graphHeight);
        ctx.lineTo(graphWidth, graphHeight);
        ctx.stroke();

        /*
         * Draw Y Axis
         */
        ctx.beginPath();
        ctx.moveTo(margin, graphHeight);
        ctx.lineTo(margin, margin - 25);
        ctx.stroke();

        //ctx.fillStyle = "green";
        // ctx.fillRect(50,25,600,425);



        /*
         * draw x & y labels
         */

        //----------- still to do ------------

        //database

        //form
        //updatable title
        //check boxes
        //fix calculation errors

        //test


    }


}

function changeBackgroundColor()
{
    var color = document.getElementById("bgcolor").value;
    document.getElementById("myCanvas").style.backgroundColor = color;
}
function changeBodyColor()
{
    var b = document.getElementById("bdcolor").value;
    document.body.style.backgroundColor = b;
}
function changeFormColor()
{
    var f = document.getElementById("formcolor").value;
    document.forms[0].style.backgroundColor = f;
}
function changeChartTitle()
{
    var c = document.getElementById("title").value;
    document.getElementById("chartTitle").innerHTML = c;
}
function changeChartTitleColor()
{
    var c = document.getElementById("titlecolor").value;
    document.getElementById('chartTitle').style.color = c;
}
function changeXAxisColor()
{
    var c = document.getElementById("xaxiscolor").value;
    document.getElementById('xAxisContainer').style.color = c;
}
function changeYAxisColor()
{
    var c = document.getElementById("yaxiscolor").value;
    document.getElementById('yAxisContainer').style.color = c;
}
function changeXAxis()
{
    var x = document.getElementById("xAxis").value;
    document.getElementById("xAxisContainer").innerHTML = x;
}
function changeYAxis()
{
    var y = document.getElementById("yAxis").value;
    document.getElementById("yAxisContainer").innerHTML = y;
}


      