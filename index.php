<!--
/*
 * Author: John McDonnell
 * Student Number: D00096987
 * Email: D0009687@student.dkit.ie
 * Course: Computing
 * Module: Rich Web Applications 1
 * CA 1
 * Due Date 15th of November 2014
 * Version Final
 */
-->
<?php
require_once 'db.php';

$query = "SELECT * FROM applesales";
//$query = "SELECT * FROM googlesales";
//$query = "SELECT * FROM linegraphchangefeatures";
$result = mysqli_query($link, $query);
$row = mysqli_fetch_assoc($result);
//$numRow = mysqli_num_rows($results);
$x = 0;
echo "<script>var data = [];var data2 = [];var data3 = [];
        var xLabel = [];</script>";

while ($row = mysqli_fetch_assoc($result)) {
    echo "<script>data[" . $x . "]=" . $row['data'] . ";";
    echo "xLabel[" . $x . "]='" . $row['xLabel'] . "';</script>";

    $x++;
}
$y = 0;
$query2 = "SELECT * FROM samsungsales";
$result2 = mysqli_query($link, $query2);
$row2 = mysqli_fetch_assoc($result2);
while ($row2 = mysqli_fetch_assoc($result2)) {
    echo "<script>data2[" . $y . "]=" . $row2['data'] . ";</script>";


    $y++;
}
$z = 0;
$query3 = "SELECT * FROM googlesales";
$result3 = mysqli_query($link, $query3);
$row3 = mysqli_fetch_assoc($result3);
while ($row3 = mysqli_fetch_assoc($result3)) {
    echo "<script>data3[" . $z . "]=" . $row3['data'] . ";</script>";


    $z++;
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">

        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="lineGraph.js" type="text/javascript"></script>
        <title>Line Graph</title>

    </head>
    <body onload="draw()">


        <div id ="titleContainer">
            <div id="chartTitle">Apple vs Samsung vs Google Sales 2014</div>





            <canvas id="myCanvas" style="">

                Your browser does not support the HTML5 canvas tag.</canvas>

            <div id="yAxisContainer">Sales <br>(Millions)</div>
            <br>
            <div id="xAxisContainer">Months</div>
            <div id="formContainer">
                <form id="lineGraphForm" name="lineGraphForm" action="">
                    <fieldset>
                        <legend>Change Line Graph</legend>

                        <label for ="title">Title: <span class="embolden"></span></label>
                        <input id="title" name="title" type="text" size="25" placeholder="Please enter graph title here"/>
                        <input type="button" value="Change" onclick="changeChartTitle()">
                        <br><br>

                        <label for ="xAxis">X Axis Label: <span class="embolden"></span></label>
                        <input id="xAxis" name="xAxis" type="text" size="25" placeholder="Change X Axis Label"/>
                        <input type="button" value="Change" onclick="changeXAxis()">
                        <br><br>

                        <label for ="yAxis">Y Axis Label: <span class="embolden"></span></label>
                        <input id="yAxis" name="yAxis" type="text" size="25" placeholder="Change Y Axis Label"/>
                        <input type="button" value="Change" onclick="changeYAxis()">
                        <br><br>

                          <label><span class="embolden"></span>Title Colour:</label>
                        <input type="color" id="titlecolor">
                        <input type="button" value="Change" onclick="changeChartTitleColor()">
                        <br>
                        <br>
                          <label><span class="embolden"></span>X Axis Colour:</label>
                        <input type="color" id="xaxiscolor">
                        <input type="button" value="Change" onclick="changeXAxisColor()">
                        <br>
                        <br>
                          <label><span class="embolden"></span>Y Axis Colour:</label>
                        <input type="color" id="yaxiscolor">
                        <input type="button" value="Change" onclick="changeYAxisColor()">
                        <br>
                        <br>
                        <label><span class="embolden"></span>Background Colour:</label>
                        <input type="color" id="bgcolor">
                        <input type="button" value="Change" onclick="changeBackgroundColor()">
                        <br>
                        <br>
                        
                         <label><span class="embolden"></span>Body Colour:</label>
                        <input type="color" id="bdcolor">
                        <input type="button" value="Change" onclick="changeBodyColor()">
                        <br>
                        <br>
                        
                         <label><span class="embolden"></span>Form Colour:</label>
                        <input type="color" id="formcolor">
                        <input type="button" value="Change" onclick="changeFormColor()">
                        <br>
                        <br>
                        
                        
                        <label for ="applesales">Apple's Line: <span class="embolden"></span></label>
                   
                        <input id="appleColor" type="button" value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'>
                        
                        <br>
                        <br>
                        
                        <label><span class="embolden"></span>Samsungs's Line: </label>
                        <input id="samsungColor" type="button" value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'>
                        
                        <br>
                        <br>
                        
                        <label><span class="embolden"></span>Google's Line: </label>
                        <input id="googleColor" type="button" value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'>
                        
                        <br>
                        <br>
                       
                    </fieldset>
<!--                        <label><span class="embolden"></span>Plot Line Colour</label>
                        <input id="plcolor" name="plcolor" type="color">
                        <input type="button" value="Change" onclick="grid()">
                        <br><br>-->
                    </fieldset>
                </form>
               
            </div>

    </body>
</html>
